/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?

	localStorage призначений для збереження даних в виділеній пам'яті системи користувача, зберігається навіть при перезавантаженні/закритті браузера. sessionStorage призначений для тимчасового збереження даних.

2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?

	не зберігати конфіденційну інформацію в localStorage, використовувати беспечний протокол передачі даних, використовувати Content Security Policy для запобігання атакам та шкідливим скриптам.

3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

	при закритті вікна браузера дані, збережені в sessionStorage видаляються.

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
   При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/

const themeBtn = document.querySelector('#theme');
const linkTheme = document.getElementById('themeStylesheet');
const savedTheme = localStorage.getItem('dataTheme');

document.addEventListener('DOMContentLoaded', () => {
	
	if (savedTheme === 'dark') {
		linkTheme.href = './css/dark-theme.css';
		themeBtn.dataset.theme = 'dark';
	} else {
		linkTheme.href = './css/lite-theme.css';
		themeBtn.dataset.theme = 'light';
	}
});

themeBtn.addEventListener('click', (event) => {

	if (event.target.dataset.theme === 'light') {
		event.target.dataset.theme = 'dark';
		linkTheme.href = './css/dark-theme.css';
	} else if (event.target.dataset.theme === 'dark') {
		event.target.dataset.theme = 'light';
		linkTheme.href = './css/lite-theme.css';
	}

	localStorage.setItem('dataTheme', `${event.target.dataset.theme}`);
})

